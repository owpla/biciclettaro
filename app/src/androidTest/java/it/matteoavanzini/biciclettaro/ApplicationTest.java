package it.matteoavanzini.biciclettaro;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    public void testDateFormat() {
        String data = "29 Mar 2016 12:00:06";
        try {
            Date date = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(data);
            assertNotNull(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}