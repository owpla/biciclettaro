package it.matteoavanzini.biciclettaro;

import android.app.AlarmManager;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ShareActionProvider;
import android.widget.Toast;

import java.util.List;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity implements
        FragmentPost.OnFragmentPostListener {

    private static final String PREFERENCE_VIEW = "preference_view";
    private static final String PREFERENCE_FILE = "preferences.xml";

    private static final String VIEW_GRID = "grid";
    private static final String VIEW_LIST = "list";

    public static final String PARAM_ARTICLE_ID = "article_id";
    public static final String DISPLAY_PROGRESS_BAR = "displayProgressBar";

    public static final String RSS_FEED = "http://biciclettaro.it/feed";
    public static final String MAIN_URL = "http://www.biciclettaro.it";
    public static final String PHONE_NUMBER = "+39 064885567";
    public static final String EMAIL_ADDRESS = "info@biciclettaro.it";

    public static final String ACTION_APP_STARTED = "it.matteoavanzini.intent.action.APP_STARTED";
    public static final String ACTION_REFRESH_POSTS = "it.matteoavanzini.biciclettaro.ACTION_REFRESH_POSTS";

    private ProgressBar progressBar;
    private AlertDialog alertDialog;
    private BiciclettaroDatabase mDatabase;
    private Toolbar mToolbar;
    protected PostAdapterReceiver mReceiver;
    private MenuItem mShareItem;

    class PostAdapterReceiver extends BootReceiver {
        public void onReceive(Context context, Intent intent) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("content");
            if (fragment instanceof FragmentPost) {
                mDatabase = new BiciclettaroDatabase(context);
                List<Post> postList = mDatabase.getPosts();

                PostAdapter postAdapter = ((FragmentPost) fragment).getAdapter();
                postAdapter.setData(postList);
                postAdapter.notifyDataSetChanged();
            }
            hideProgressBar();
        }
    }

    protected void hideProgressBar() {

        if (progressBar!=null && mDatabase!=null) {
            List<Post> listPost = mDatabase.getPosts();
            if (listPost.size() > 0) {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        onCreateActionBar();

        alertDialog = new AlertDialog.Builder(this).create();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mDatabase = new BiciclettaroDatabase(this);
        mReceiver = new PostAdapterReceiver();

        Intent bootReceiverIntent = new Intent(MainActivity.ACTION_APP_STARTED);
        sendBroadcast(bootReceiverIntent);
    }

    protected void onCreateActionBar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitleTextColor(0xFFFFFFFF);
        mToolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_more_vert_24dp));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_24dp);
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(ACTION_REFRESH_POSTS);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(mReceiver, filter);

        if (InternetConnection.haveInternetConnection(MainActivity.this)) {

            hideProgressBar();

            int postId = getIntent().getIntExtra(PARAM_ARTICLE_ID, 0);
            if (postId != 0) {
                List<Post> listPost = mDatabase.getPosts();
                Post post = mDatabase.getPost(postId);
                int position = listPost.indexOf(post);
                Fragment single = getFragmentPager(position);
                addFragment(single);
            } else {
                loadMainFragment();
            }

        } else {
            displayDialogInternetConnectionError();
        }
    }

    @Override
    public void onPause() {
        unregisterReceiver(mReceiver);
        super.onPause();
    }

    private void displayDialogInternetConnectionError() {
        alertDialog.setTitle(getResources().getString(R.string.error));
        alertDialog.setMessage(getResources().getString(R.string.no_internet_connection));
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
                getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    private String getViewPreference() {
        SharedPreferences preferences = getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        String preferenceView = preferences.getString(PREFERENCE_VIEW, VIEW_LIST);
        return preferenceView;
    }

    private void loadMainFragment() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        String preferenceView = getViewPreference();
        if (preferenceView.equals(VIEW_LIST)) {
            attachListFragment();
        } else {
            attachGridFragment();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar, menu);

        MenuItem itemSwitchView = menu.findItem(R.id.switch_view);
        String preferenceView = getViewPreference();
        String preferenceViewToDisplay;
        if (preferenceView.equals(VIEW_LIST)) {
            preferenceViewToDisplay = VIEW_GRID;
        } else {
            preferenceViewToDisplay = VIEW_LIST;
        }

        mShareItem = menu.findItem(R.id.share);
        mShareItem.setVisible(false);

        setMenuItem(itemSwitchView, preferenceViewToDisplay);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.share:
                shareAction();
                return true;
            case R.id.website:
                goToSiteAction();
                return true;
            case R.id.call:
                callNumberAction();
                return true;
            case R.id.email:
                sendEmailAction();
                return true;
            case R.id.switch_view:
                toggleView(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("content");
        if (fragment instanceof FragmentPostSingle ||
                fragment instanceof FragmentPager) {
            loadMainFragment();
        } else {
            finish();
        }
    }

    public Fragment getCurrentFragment() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("content");
        return currentFragment;
    }

    private boolean isArticleFragment(Fragment fragment) {
        if (fragment instanceof FragmentPostSingle || fragment instanceof FragmentPager) {
            return true;
        }
        return false;
    }
    @Override
    public void addFragment(Fragment fragment) {

        if (mShareItem!=null) mShareItem.setVisible(false);
        if (isArticleFragment(fragment)) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (mShareItem!=null) mShareItem.setVisible(true);
        }

        Fragment currentFragment = getCurrentFragment();
        if (currentFragment!=null  && fragment.getClass().equals(currentFragment.getClass())) {
            return;
        }

        FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null);
        if (currentFragment == null) {
            ft.add(R.id.mainview, fragment, "content");
        } else {
            ft.replace(R.id.mainview, fragment, "content");
        }
        ft.commit();
    }

    private void setPreferenceView(String viewPreference) {
        SharedPreferences preferences = getSharedPreferences(PREFERENCE_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREFERENCE_VIEW, viewPreference);
        editor.commit();
    }

    private void toggleView(MenuItem item) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag("content");

        if (currentFragment instanceof FragmentPostList) {
            setPreferenceView(MainActivity.VIEW_GRID);
            setMenuItem(item, VIEW_LIST);
            attachGridFragment();
        } else {
            setPreferenceView(MainActivity.VIEW_LIST);
            setMenuItem(item, VIEW_GRID);
            attachListFragment();
        }
    }

    private void setMenuItem(MenuItem item, String preferenceView) {

        String title;
        int icon;

        String gridTitle = getResources().getString(R.string.grid);
        String listTitle = getResources().getString(R.string.list);
        if (preferenceView.equals(VIEW_LIST)) {
            title = listTitle;
            icon = R.drawable.ic_list_24dp;
        } else {
            title = gridTitle;
            icon = R.drawable.ic_grid_24dp;
        }

        item.setTitle(title);
        item.setIcon(icon);
    }

    private void attachGridFragment() {
        FragmentPostGrid f = FragmentPostGrid.getInstance();
        addFragment(f);
    }

    private void attachListFragment() {
        FragmentPostList f = FragmentPostList.getInstance();
        addFragment(f);
    }

    @Override
    public Fragment getFragmentPager(int position) {
        Fragment single = FragmentPager.getInstance(position);
        return single;
    }

    private Post getCurrentPost() {
        Fragment currentFragment = getCurrentFragment();
        List<Post> postList = mDatabase.getPosts();
        int position = ((FragmentPager) currentFragment).mPager.getCurrentItem();
        Post post = postList.get(position);
        return post;
    }

    public void callNumberAction() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + PHONE_NUMBER));
        startActivity(intent);
    }

    public void goToSiteAction() {
        Fragment currentFragment = getCurrentFragment();
        String url = MainActivity.MAIN_URL;
        if (isArticleFragment(currentFragment)) {
            Post post = getCurrentPost();
            url = post.getUrl();
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    public void shareAction() {
        Post post = getCurrentPost();
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, post.getUrl());
        startActivity(Intent.createChooser(intent, getResources().getText(R.string.send_to)));
    }

    public void sendEmailAction() {
        Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{EMAIL_ADDRESS});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Richiesta informazioni");
        intent.putExtra(Intent.EXTRA_TEXT, "");

        Intent chooser = Intent.createChooser(intent, getResources().getText(R.string.send_email));
        try {
            startActivity(chooser);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

}
