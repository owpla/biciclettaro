package it.matteoavanzini.biciclettaro;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import java.util.List;

public class FragmentPost extends Fragment {

    protected interface OnFragmentPostListener {
        public void addFragment(Fragment fragment);
        public Fragment getFragmentPager(int position);
    }

    protected BiciclettaroDatabase mDatabase;
    protected List<Post> mData;
    protected Context mContext;
    protected OnFragmentPostListener mFragmentPostListener;
    protected PostAdapter mPostAdapter;


    protected AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Fragment single = mFragmentPostListener.getFragmentPager(position);
            mFragmentPostListener.addFragment(single);
        }
    };

    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mContext = getActivity();
        mDatabase = new BiciclettaroDatabase(mContext);
        mData = mDatabase.getPosts();
        mFragmentPostListener = (MainActivity) getActivity();
    }

    public PostAdapter getAdapter() {
        return mPostAdapter;
    }


}
