package it.matteoavanzini.biciclettaro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class FragmentPostList extends FragmentPost {

    ListView mList;

    public static FragmentPostList getInstance() {
        FragmentPostList f = new FragmentPostList();
        return f;
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_list, null);

        mList = (ListView) layout.findViewById(R.id.list);
        mList.setOnItemClickListener(mItemClickListener);

        return layout;
    }

    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPostAdapter = new PostAdapter(mContext, R.layout.post_item_list, mData);
        mList.setAdapter(mPostAdapter);
    }

}

