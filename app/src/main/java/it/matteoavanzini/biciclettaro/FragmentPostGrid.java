package it.matteoavanzini.biciclettaro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

public class FragmentPostGrid extends FragmentPost {

    private GridView mGridView;

    public static FragmentPostGrid getInstance() {
        FragmentPostGrid f = new FragmentPostGrid();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_grid, null);

        mGridView = (GridView) layout.findViewById(R.id.grid);
        mGridView.setOnItemClickListener(mItemClickListener);

        return layout;
    }

    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mPostAdapter = new PostAdapter(mContext, R.layout.post_item_grid, mData);
        mGridView.setAdapter(mPostAdapter);
    }
}

