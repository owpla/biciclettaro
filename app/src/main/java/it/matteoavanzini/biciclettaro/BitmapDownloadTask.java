package it.matteoavanzini.biciclettaro;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class BitmapDownloadTask extends AsyncTask<String, Integer, Bitmap> {

    private Context mContext;
    private String mFilename;

    public BitmapDownloadTask(Context context) {
        this.mContext = context;
    }

    /**
     * Vuole due parametri:
     * @param String sUrl[0] url dell'immagine da scaricare
     * @param String sUrl[1] nome del file da salvare
     * */
    @Override
    protected Bitmap doInBackground(String... sUrl) {
        InputStream input = null;
        HttpURLConnection connection = null;
        Bitmap bitmap = null;
        try {
            mFilename = sUrl[1];
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.e(MainActivity.class.getName(), "Connection error");
                return null;
            }

            input = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            Log.e(MainActivity.class.getName(), e.toString());
            bitmap = null;
        } finally {
            try {
                if (input != null)
                    input.close();
            } catch (IOException ignored) {}

            if (connection != null)
                connection.disconnect();
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        saveBitmapFile(result, mFilename);
    }

    protected void saveBitmapFile(Bitmap bmp, String filename) {
        FileOutputStream fileOut = null;

        try{
            //File externalStorage = getAlbumStorageDir(mContext, "Biciclettaro");
            //fileOut = new FileOutputStream(externalStorage.getPath() + "/" + filename);
            fileOut = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fileOut);
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } finally {
            try{
                if(fileOut != null){
                    fileOut.close();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public File getAlbumStorageDir(Context context, String albumName) {
        // Get the directory for the app's private pictures directory.
        File file = new File(context.getExternalFilesDir(
                Environment.DIRECTORY_PICTURES), albumName);
        if (!file.mkdirs()) {
            Log.e(BitmapDownloadTask.class.getName(), "Directory not created");
        }
        return file;
    }
}




