package it.matteoavanzini.biciclettaro;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class FragmentPager extends Fragment {

    private static final String POSITION = "position";
    ViewPager mPager;
    BiciclettaroDatabase mDatabase;

    public static FragmentPager getInstance(int position) {
        FragmentPager fragment = new FragmentPager();
        Bundle args = new Bundle();
        args.putInt(POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_pager, container, false);

        mDatabase = new BiciclettaroDatabase(getActivity());
        List<Post> posts = mDatabase.getPosts();
        PostPagerAdapter adapter = new PostPagerAdapter(getActivity(), posts, getFragmentManager());
        mPager = (ViewPager) layout.findViewById(R.id.viewPager);
        mPager.setAdapter(adapter);
        int selectedPosition = getArguments().getInt(POSITION, 0);
        mPager.setCurrentItem(selectedPosition);

        return layout;
    }


}
