package it.matteoavanzini.biciclettaro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by emme on 05/04/16.
 */
public class FragmentPostSingle extends Fragment {

    public static final String POST = "post";

    public static FragmentPostSingle getInstance(Post post) {
        FragmentPostSingle f = new FragmentPostSingle();
        Bundle args = new Bundle();
        args.putParcelable(POST, post);
        f.setArguments(args);
        return f;
    }

    private Post mPost;

    private void getPostFromArguments() {
        if (getArguments() != null && getArguments().containsKey(POST)) {
            mPost = getArguments().getParcelable(POST);
        }
    }

    public Post getPost() {
        return mPost;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPostFromArguments();
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_post_item, null);

        ImageView image = (ImageView) layout.findViewById(R.id.image);
        TextView title  = (TextView) layout.findViewById(R.id.title);
        TextView content  = (TextView) layout.findViewById(R.id.content);

        image.setImageBitmap(mPost.getImage());
        String titleLink = "<a href=\"" + mPost.getUrl() + "\">" + mPost.getTitle() + "</a>";
        title.setText(mPost.getTitle());
        Log.d(FragmentPostSingle.class.getName(), titleLink);

        content.setText(Html.fromHtml(mPost.getContent()));
        content.setMovementMethod(LinkMovementMethod.getInstance());

        return layout;
    }
}
