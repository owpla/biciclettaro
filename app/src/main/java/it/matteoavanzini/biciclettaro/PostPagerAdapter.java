package it.matteoavanzini.biciclettaro;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by emme on 05/04/16.
 */
public class PostPagerAdapter extends FragmentStatePagerAdapter {

    List<Post> mPosts;

    public PostPagerAdapter(Context context, List<Post> posts, FragmentManager fragmentManager) {
        super(fragmentManager);
        mPosts = posts;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return mPosts.size();
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            default:
                Post post = mPosts.get(position);
                Fragment single = FragmentPostSingle.getInstance(post);
                return single;
        }
    }

}
