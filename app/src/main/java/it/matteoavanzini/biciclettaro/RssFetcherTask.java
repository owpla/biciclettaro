package it.matteoavanzini.biciclettaro;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RSSFetcherTask extends AsyncTask<String, Integer, List<Post>> {

    Context mContext;

    final String LOG_TAG = RSSFetcherTask.class.getName();

    RSSFetcherTask(Context context) {
        mContext = context;
    }

    @Override
    protected List<Post> doInBackground(String... sUrl) {
        Log.i(LOG_TAG, "Fetching XML...");
        InputStream input = null;
        HttpURLConnection connection = null;
        List<Post> postList = null;
        try {
            URL url = new URL(sUrl[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(10000);
            connection.connect();

            input = connection.getInputStream();

            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(input, null);
            parser.nextTag(); // rss
            parser.nextTag(); // channel
            postList = readFeed(parser);

        } catch (Exception err) {
            Log.e(RSSFetcherTask.class.getName(), err.getMessage().toString());
        } finally {
            try {
                if (connection!=null) connection.disconnect();
                if (input!=null) input.close();
            } catch (IOException error) {}
        }
        return postList;
    }

    @Override
    public void onPostExecute(List<Post> postList) {

        Post lastPost = null;

        BiciclettaroDatabase database = new BiciclettaroDatabase(mContext);
        int notificationNumber = 0;
        if (postList!=null) {
            for (Post post : postList) {
                if (database.isValid(post) &&
                        (!database.exists(post) || database.isNewer(post) )) {
                    //Log.i(RSSFetcherTask.class.getName(), "Salvo post: " +  post.getTitle());

                    database.savePost(post);
                    notificationNumber++;
                    lastPost = post;

                }
            }
        }

        if (isActivityInForeground()) {
            Intent adapterReceiverIntent = new Intent(MainActivity.ACTION_REFRESH_POSTS);
            mContext.sendBroadcast(adapterReceiverIntent);
        } else if (notificationNumber > 0){
            notify(notificationNumber, lastPost);
        }
    }

    private boolean isActivityInForeground() {
        ActivityManager am = (ActivityManager) mContext.getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("current task :", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClass().getSimpleName());
        ComponentName componentInfo = taskInfo.get(0).topActivity;
        if(componentInfo.getPackageName().equalsIgnoreCase("it.matteoavanzini.biciclettaro")){
            return true;
        }
        return false;
    }

    private List<Post> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Post> postList = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, null, "channel");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("item")) {
                postList.add(readPost(parser));
            } else {
                skip(parser);
            }
        }

        return postList;
    }

    // Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
    // to their respective "read" methods for processing. Otherwise, skips the tag.
    private Post readPost(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, null, "item");
        String title = null;
        String content = null;
        String url = null;
        String pubDate = null;
        String imageName = null;
        String imageUrl = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                title = readTagContent(parser, "title");
            } else if (name.equals("content:encoded")) {
                content = readTagContent(parser, "content:encoded");
                imageUrl = readImageUrl(content);
                //imageUrl = imageUrl.replace("-150x150", "");
                imageName = readImageName(imageUrl);
                content = excludeTags(content, "img");
                // download dell'immagine
                Bitmap bitmap = downloadBitmap(imageUrl);
                if (bitmap != null) {
                    saveBitmapFile(bitmap, imageName);
                }

            } else if (name.equals("link")) {
                url = readTagContent(parser, "link");
            } else if (name.equals("pubDate")) {
                pubDate = readTagContent(parser, "pubDate");
                try {
                    pubDate = pubDate.substring(pubDate.indexOf(",") + 1, pubDate.indexOf("+"));
                    Date date = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(pubDate);
                    pubDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
                    //Log.d(LOG_TAG, pubDate);
                } catch (ParseException e) {
                    Log.e(LOG_TAG, e.getMessage());
                }

            } else {
                skip(parser);
            }
        }

        //Log.i(RSSFetcherTask.class.getName(), "Scaricato post: " + title);
        return new Post(0, title, content, url, pubDate, imageName, imageUrl, null);
    }

    private String excludeTags(String content, String... tags) {
        for (String tag : tags) {
            int indexStart = content.indexOf("<"+tag);
            int autoCloseIndexTag = content.indexOf("/>", indexStart);
            int closeIndexTag = content.indexOf("</"+tag+">", indexStart);

            int indexEnd;
            if (autoCloseIndexTag>-1 && closeIndexTag>-1) {
                if (closeIndexTag < autoCloseIndexTag) {
                    indexEnd = getNormalCloseIndexTagEnd(closeIndexTag, tag);
                } else {
                    indexEnd = getAutoCloseIndexTagEnd(autoCloseIndexTag);
                }
            } else if (autoCloseIndexTag > -1){
                indexEnd = getAutoCloseIndexTagEnd(autoCloseIndexTag);
            } else {
                indexEnd = getNormalCloseIndexTagEnd(closeIndexTag, tag);
            }

            String tagContent = content.substring(indexStart, indexEnd);
            content = content.replace(tagContent, "");
        }
        return content;
    }

    private int getAutoCloseIndexTagEnd(int autoCloseIndexTag) {
        return autoCloseIndexTag + 2;
    }

    private int getNormalCloseIndexTagEnd(int closeIndexTag, String tag) {
        return closeIndexTag + 2 + tag.length();
    }

    private Bitmap downloadBitmap(String bitmapUrl) {
        InputStream input = null;
        HttpURLConnection connection = null;
        Bitmap bitmap = null;
        try {
            URL url = new URL(bitmapUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.e(MainActivity.class.getName(), "Connection error");
                return null;
            }

            input = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            Log.e(MainActivity.class.getName(), e.toString());
            bitmap = null;
        } finally {
            try {
                if (input != null)
                    input.close();
            } catch (IOException ignored) {}

            if (connection != null)
                connection.disconnect();
        }
        return bitmap;
    }

    protected void saveBitmapFile(Bitmap bmp, String filename) {
        FileOutputStream fileOut = null;

        try{
            //File externalStorage = getAlbumStorageDir(mContext, "Biciclettaro");
            //fileOut = new FileOutputStream(externalStorage.getPath() + "/" + filename);
            //Log.i(RSSFetcherTask.class.getName(), "Saving image " + filename);
            fileOut = mContext.openFileOutput(filename, Context.MODE_PRIVATE);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fileOut);
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } finally {
            try{
                if(fileOut != null){
                    fileOut.close();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    private String readTagContent(XmlPullParser parser, String tagName) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, tagName);
        String content = readText(parser);
        parser.require(XmlPullParser.END_TAG, null, tagName);
        return content;
    }

    private String readImageName(String url) {
        String [] tmp = url.split("/");
        return tmp[tmp.length - 1];
    }

    private String readImageUrl(String description) {
        int srcIndex = 5 + description.indexOf("src=");
        int endIndex = description.indexOf("\"", srcIndex);
        String imageUrl = description.substring(srcIndex, endIndex);
        return imageUrl;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private void notify(int number, Post post) {

        String title = post.getTitle();
        String description = Html.fromHtml(post.getContent()).toString();
        int articleId = post.getId();

        if (number > 1) {
            title = mContext.getResources().getString(R.string.app_name);
            description = String.format(mContext.getResources().getString(R.string.new_messages), number);
            articleId = 0;
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(mContext)
                        .setSmallIcon(R.drawable.ic_bike_24dp)
                        .setContentTitle(title)
                        .setContentText(description)
                        .setNumber(number)
                        .setAutoCancel(true);

        Intent resultIntent = new Intent(mContext, MainActivity.class);
        resultIntent.putExtra(MainActivity.PARAM_ARTICLE_ID, articleId);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent =
                PendingIntent.getActivity(mContext, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }
}

