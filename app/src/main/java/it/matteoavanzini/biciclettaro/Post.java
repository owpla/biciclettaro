package it.matteoavanzini.biciclettaro;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

public class Post implements Parcelable {

    private int id;
    private String title;
    private String content;
    private String url;
    private String imageName;
    private String imageUrl;
    private String pubDate;
    private Bitmap image;

    public Post(int id, String title, String content, String url, String pubDate, String imageName, String imageUrl, Bitmap bitmap) {
        setId(id);
        setTitle(title);
        setContent(content);
        setUrl(url);
        setPubDate(pubDate);
        setImageName(imageName);
        setImageUrl(imageUrl);
        setImage(bitmap);
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() { return imageUrl; }

    public void setImageUrl(String imageUrl) { this.imageUrl = imageUrl; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeString(url);
        dest.writeString(imageName);
        dest.writeString(imageUrl);
        dest.writeString(pubDate);
        dest.writeParcelable(image, 0);
    }

    private Post(Parcel in) {
        id = in.readInt();
        title = in.readString();
        content = in.readString();
        url = in.readString();
        imageName = in.readString();
        imageUrl = in.readString();
        pubDate = in.readString();
        image = in.readParcelable(Bitmap.class.getClassLoader());
    }

    public static final Parcelable.Creator CREATOR =
            new Parcelable.Creator() {
                public Post createFromParcel(Parcel in) {
                    return new Post(in);
                }
                public Post[] newArray(int size) {
                    return new Post[size];
                }
            };

}

