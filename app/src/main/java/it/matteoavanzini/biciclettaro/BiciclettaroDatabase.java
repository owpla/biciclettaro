package it.matteoavanzini.biciclettaro;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BiciclettaroDatabase extends SQLiteOpenHelper {
    public static final String DATABASE_FILENAME = "biciclettaro.db3";
    public static final int DATABASE_VERSION = 1;
    private Context mContext;

    public BiciclettaroDatabase(Context context){
        super(context, DATABASE_FILENAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(PostTable.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean hasPosts() {
        List<Post> postList = getPosts();
        return postList.size() > 0 ? true : false;
    }

    public boolean exists(Post post) {
        int postId = getPostIdIfExists(post.getUrl());
        return postId!=0 ? true : false;
    }

    public int getPostIdIfExists(String url) {
        int postId = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(PostTable.TABLE_NAME, null, PostTable.URL+"=?", new String[] { url },
                null, null, null );
        if (c.moveToNext()) {
            postId = c.getInt(c.getColumnIndex(PostTable._ID));
        }
        db.close();
        return postId;
    }

    public boolean isValid(Post post) {
        boolean ret = true;
        if ((post.getTitle()== null || post.getTitle().isEmpty()) ||
                (post.getContent()== null || post.getContent().isEmpty()) ||
                (post.getUrl()== null || post.getUrl().isEmpty())) {
            ret = false;
        }

        return ret;
    }

    public boolean isNewer(Post post) {
        boolean ret = false;
        int id = getPostIdIfExists(post.getUrl());
        if (id == 0) {
            return true;
        } else {
            Post oldPost = getPost(id);
            try {
                Date dateOldPost = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(oldPost.getPubDate());
                Date datePost = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(post.getPubDate());

                if (datePost.after(dateOldPost)) {
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return ret;
    }

    public boolean savePost(Post post) {
        boolean retVal = true;

        ContentValues values = new ContentValues();
        values.put(PostTable.TITLE, post.getTitle());
        values.put(PostTable.CONTENT, post.getContent());
        values.put(PostTable.IMAGE_NAME, post.getImageName());
        values.put(PostTable.IMAGE_URL, post.getImageUrl());
        values.put(PostTable.URL, post.getUrl());
        values.put(PostTable.PUB_DATE, post.getPubDate());


        int postId = getPostIdIfExists(post.getUrl());
        post.setId(postId);

        SQLiteDatabase db = this.getWritableDatabase();
        try {
            if (postId == 0) {
                db.insert(PostTable.TABLE_NAME, null, values);
            } else {
                db.update(PostTable.TABLE_NAME, values, PostTable._ID + "=?",
                        new String[]{Integer.toString(post.getId())});
            }
        } catch (Exception err) {
            retVal = false;
        }
        db.close();

        return retVal;
    }

    public Post getPost(int postId) {
        Post post = null;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(PostTable.TABLE_NAME, null, PostTable._ID+"=?", new String[] { Integer.toString(postId) }, null, null, null );
        while (c.moveToNext()) {
            post = getPost(c);
        }
        db.close();
        return post;
    }

    public List<Post> getPosts() {
        List<Post> postList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.query(PostTable.TABLE_NAME, null, null, null, null, null, PostTable.PUB_DATE + " DESC" );
        while (c.moveToNext()) {
            Post post = getPost(c);
            postList.add(post);
        }
        db.close();
        return postList;
    }

    private Post getPost(Cursor c) {
        int id = c.getInt(c.getColumnIndex(PostTable._ID));
        String title = c.getString(c.getColumnIndex(PostTable.TITLE));
        String content = c.getString(c.getColumnIndex(PostTable.CONTENT));
        String imageName = c.getString(c.getColumnIndex(PostTable.IMAGE_NAME));
        String imageUrl = c.getString(c.getColumnIndex(PostTable.IMAGE_URL));
        String url = c.getString(c.getColumnIndex(PostTable.URL));
        String pubDate = c.getString(c.getColumnIndex(PostTable.PUB_DATE));

        Bitmap bitmap = null;
        try {
            bitmap = getBitmapFromFile(imageName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Post post = new Post(id, title, content, url, pubDate, imageName, imageUrl, bitmap);
        return post;
    }

    private Bitmap getBitmapFromFile(String filename) throws FileNotFoundException {
        FileInputStream bitmapFile = mContext.openFileInput(filename);
        Bitmap bitmap = BitmapFactory.decodeStream(bitmapFile);
        return bitmap;
    }
}