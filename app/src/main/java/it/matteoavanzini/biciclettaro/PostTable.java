package it.matteoavanzini.biciclettaro;

import android.provider.BaseColumns;

/**
 * Created by emme on 12/03/16.
 */
public class PostTable implements BaseColumns {

    public static final String TITLE = "title";
    public static final String CONTENT = "content";
    public static final String URL = "url";
    public static final String IMAGE_NAME = "image_name";
    public static final String IMAGE_URL = "image_url";
    public static final String PUB_DATE = "pub_date";

    public static final String TABLE_NAME = "posts";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + TITLE + " TEXT,"
                    + CONTENT + " TEXT, "
                    + URL + " TEXT, "
                    + IMAGE_NAME + " TEXT, "
                    + IMAGE_URL + " TEXT, "
                    + PUB_DATE + " TEXT "
                    + ")";
}
