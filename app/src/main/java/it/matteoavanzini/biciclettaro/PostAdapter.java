package it.matteoavanzini.biciclettaro;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PostAdapter extends ArrayAdapter<Post> {

    Context mContext;
    List<Post> mData;
    int mResourceLayout;

    public PostAdapter(Context context, int resourceLayout, List<Post> data) {
        super(context, 0);
        mContext = context;
        mData = data;
        mResourceLayout = resourceLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Post post = mData.get(position);
        ViewHolder holder;
        if (convertView == null) {
            convertView =
                    LayoutInflater.from(mContext).inflate(mResourceLayout, parent, false);

            holder = new ViewHolder();

            holder.title = (TextView) convertView.findViewById(R.id.title_text);
            holder.image = (ImageView) convertView.findViewById(R.id.post_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(post.getTitle());
        if (post.getImage() != null) {
            holder.image.setImageBitmap(post.getImage());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    public void setData(List<Post> posts) {
        mData = posts;
    }

    static class ViewHolder {
        TextView title;
        ImageView image;
    }
}

